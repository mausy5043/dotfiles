#!/usr/bin/env bash

HERE=$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)
FILES="${HERE}/archlinux/*"

touch "$HOME/.alias"
touch "$HOME/.funcs"
touch "$HOME/.paths"

for filepath in $FILES; do
  file=$(basename "$filepath")
  # Files
  echo -n "- Processing .$file  "
  if [ -e "${HOME}/.${file}" ]; then
    if [ -L "${HOME}/.${file}" ]; then
      echo "  .$file exists as a link. Ignoring."
    else
      echo "  .$file exists as a regular file."
      echo -n "      Moving out of the way: "
      mv -v "${HOME}/.${file}" "${HOME}/.${file}.bak"
      echo -n "      Creating link        : "
      ln -sv "${filepath}" "${HOME}/.${file}"
    fi
  fi
done

# install a service that sends a mail if a service fails
sudo mkdir -p /etc/systemd/system/service.d
sudo cp "$HERE/common/systemd/toplevel-override.conf" /etc/systemd/system/service.d/toplevel-override.conf
sudo cp "$HERE/common/systemd/failure-notification@.service" /etc/systemd/system/failure-notification@.service
sudo mkdir -p /usr/local/bin
sudo ln -s "$HERE/pios/bin/systemd-fail.sh" /usr/local/bin/failure-notification.sh
sudo chmod -Rv -x /etc/systemd/system

# install `dig`
sudo pacman -Sy extra/bind
