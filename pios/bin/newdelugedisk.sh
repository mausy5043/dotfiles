#!/bin/bash

devicepath=/dev/sda1
mountpath=/mnt/icybox

sudo mount ${devicepath} ${mountpath} || echo "Already mounted"
sudo mkdir -p ${mountpath}/tortemp
sudo mkdir -p ${mountpath}/torrent/watchme
sudo mkdir -p ${mountpath}/torrent
sudo mkdir -p ${mountpath}/log
sudo chown -R debian-deluged:debian-deluged ${mountpath}/*
sudo chmod -R 775 ${mountpath}/*
