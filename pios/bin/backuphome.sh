#!/bin/bash

if [ ! -f "${HOME}/.config/.rsync.secret" ]; then
  echo "secret not found"
  exit 0
fi

# MEDIACENTER=$(systemctl list-unit-files |grep enabled |grep mediacenter |wc -c)
CLNT=$(hostname)
OPTS="--archive \
--bwlimit=10000 \
--delete-excluded \
--delete-during \
--exclude-from=${HOME}/.rsync/exclude.txt \
--ignore-errors \
--include-from=${HOME}/.rsync/include.txt \
--itemize-changes \
--no-g \
--one-file-system \
--owner \
--password-file=${HOME}/.config/.rsync.secret \
--perms \
--recursive \
--stats \
--super \
--times \
--verbose \
--whole-file \
"

# synchronise disks and empty any diskbuffers
sync; sync

# perform the backup
# shellcheck disable=SC2086
rsync ${OPTS}  /home/  "rsync://asgard/backup/$CLNT/home"

# shellcheck disable=SC2086
# rsync ${OPTS}  /etc/   "rsync://asgard/backup/$CLNT/etc"

if [[ -e /mnt/icybox ]]; then
  echo "ICYBOX installed"
  # shellcheck disable=SC2086
  rsync ${OPTS}  /mnt/icybox/deluge/config/   "rsync://asgard/backup/$CLNT/deluge-config"
fi
wait

logger -p local7.info -t backuphome "Backup finished"
