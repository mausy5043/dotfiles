#!/bin/sh

skeel=/sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq
opt=/opt/vc/bin/vcgencmd
cpu=/sys/class/thermal/thermal_zone0/temp

if [ -e "$skeel" ]; then
  cat "$skeel"
fi
if [ -e "$opt" ]; then
  /opt/vc/bin/vcgencmd measure_temp
fi
if [ -e "$cpu" ]; then
  cat ${cpu}
fi

df -h

free -h

uptime
