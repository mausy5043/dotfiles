#!/usr/bin/env python3

import datetime

import pytz as pt
import requests
import skyfield as sf
from skyfield import almanac, api

print(f"Using skyfield {sf.VERSION}")
now_year: int = datetime.date.today().year
ystr_year: int = now_year - 1
next_year: int = now_year + 1

mytz = pt.timezone("Europe/Amsterdam")
url = "https://api.buienradar.nl/data/public/1.1/jsonfeed"

ts = api.load.timescale()
ephem = api.load("de440s.bsp")
# print(ephem)

print(" ")

t0 = ts.utc(ystr_year, 1, 1)
t1 = ts.utc(next_year, 12, 31)
t, y = almanac.find_discrete(t0, t1, almanac.seasons(ephem))

for yi, ti in zip(y, t, strict=False):
    if yi == 0:
        print("")
    print(
        f"{yi} {almanac.SEASON_EVENTS[yi]} \t:  {
            str(ti.astimezone(mytz)).split('.', maxsplit=1)[0]
        }"
    )

print(" ")

location = api.Topos("51.5806 N", "5.0117 E")


t0 = ts.utc(now_year, 1, 1, 4)
t1 = ts.utc(now_year, 12, 31, 4)
t, y = almanac.find_discrete(t0, t1, almanac.sunrise_sunset(ephem, location))

for k, sun_state in enumerate(y):
    sunrise = sun_state
    if not sunrise:
        a = t[k - 1].astimezone(mytz)
        b = t[k].astimezone(mytz)
        d = (b - a) / 2 + a
        print(
            f"{str(a.astimezone(mytz)).split('.', maxsplit=1)[0]}   -> "
            f"{str(d.astimezone(mytz)).split('.', maxsplit=1)[0].split(' ')[1]}   --> "
            f"{str(b.astimezone(mytz)).split('.', maxsplit=1)[0].split(' ')[1]}"
        )

tn = ts.now()
# pyright: reportIndexIssue=false
ttoday = ts.utc(tn.utc[0], tn.utc[1], tn.utc[2], 3)
ttomor = ts.utc(tn.utc[0], tn.utc[1], tn.utc[2] + 1, 3)

t, y = almanac.find_discrete(ttoday, ttomor, almanac.sunrise_sunset(ephem, location))

print("\n-----")
for yi, ti in zip(y, t, strict=False):
    if yi:
        a = ti.astimezone(mytz)
    else:
        b = ti.astimezone(mytz)
# pyright: reportOperatorIssue=false
d = (b - a) / 2 + a

print(f"Zon op   :    {str(a).split()[1].split('.', maxsplit=1)[0]}")
print(f"Zon hoog :    {str(d).split()[1].split('.', maxsplit=1)[0]}")
print(f"Zon onder:    {str(b).split()[1].split('.', maxsplit=1)[0]}")

with requests.get(url, timeout=10) as response:
    data = response.json()
stns = data["buienradarnl"]["weergegevens"]["actueel_weer"]["weerstations"]["weerstation"]

sampletime = "../../.... ..:..:.."
temperatuur = 15.0
solrad = "xx"
pressure = 1013.15
for stn in stns:
    # print(stn['@id'], stn['stationnaam'])
    if int(stn["@id"]) == 6350:
        # print(stn)
        for key in stn:
            if key == "datum":
                sampletime = stn[key]
            if key == "temperatuurGC":
                temperatuur = float(stn[key])
            if key == "zonintensiteitWM2":
                solrad = stn[key]
            if key == "luchtdruk":
                pressure = float(stn[key])

print(f"\n{sampletime}   :   {temperatuur} degC / {pressure} mbar /  {solrad} W/m2")


sun = ephem["Sun"]
earth = ephem["Earth"]

# Compute the sun position as seen from the observer at <location>
sun_pos = (earth + location).at(ts.now()).observe(sun).apparent()
# Compute apparent altitude & azimuth for the sun's position
altitude, azimuth, distance = sun_pos.altaz(temperature_C=temperatuur, pressure_mbar=pressure)

# Print results
print()
print(f"Altitude: {altitude.degrees:.4f} °")
print(f"Azimuth: {azimuth.degrees:.4f} °")
