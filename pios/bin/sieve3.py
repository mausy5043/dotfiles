#!/usr/bin/env python3

import datetime
import sys

import numpy as np


def eratosthenes2(n):
    multiples = set()
    for i in range(2, n + 1):
        if i not in multiples:
            yield i
            multiples.update(list(range(i * i, n + 1, i)))


def primes_upto(limit):
    is_prime = [False] * 2 + [True] * (limit - 1)
    for n in range(int(limit**0.5 + 1.5)):  # stop at ``sqrt(limit)``
        if is_prime[n]:
            for i in range(n * n, limit + 1, n):
                is_prime[i] = False
    return [i for i, prime in enumerate(is_prime) if prime]


def iprimes_upto(limit):
    is_prime = [False] * 2 + [True] * (limit - 1)
    for n in range(int(limit**0.5 + 1.5)):  # stop at ``sqrt(limit)``
        if is_prime[n]:
            for i in range(n * n, limit + 1, n):  # start at ``n`` squared
                is_prime[i] = False
    for i in range(limit + 1):
        if is_prime[i]:
            yield i


def primes2(limit):
    if limit < 2:
        return []
    if limit < 3:
        return [2]
    lmtbf = (limit - 3) // 2
    buf = [True] * (lmtbf + 1)
    for i in range((int(limit**0.5) - 3) // 2 + 1):
        if buf[i]:
            p = i + i + 3
            s = p * (i + 1) + i
            buf[s::p] = [False] * ((lmtbf - s) // p + 1)
    return [2] + [i + i + 3 for i, v in enumerate(buf) if v]


def iprimes2(limit):
    yield 2
    if limit < 3:
        return
    lmtbf = (limit - 3) // 2

    buf = [True] * (lmtbf + 1)
    for i in range((int(limit**0.5) - 3) // 2 + 1):
        if buf[i]:
            p = i + i + 3
            s = p * (i + 1) + i
            buf[s::p] = [False] * ((lmtbf - s) // p + 1)
    for i in range(lmtbf + 1):
        if buf[i]:
            yield i + i + 3


def primes_upto2(limit):
    is_prime = np.ones(limit + 1, dtype=bool)
    for n in range(2, int(limit**0.5 + 1.5)):
        if is_prime[n]:
            is_prime[n * n :: n] = 0
    return np.nonzero(is_prime)[0][2:]


# Check if the script was called with a
# parameter. Use that as the upper limit
# of numbers to search
limt = 1000 if len(sys.argv) != 2 else int(sys.argv[1])

print("Sieve of Eratosthenes\n")

# Record the current time
startTime = datetime.datetime.now()
result = list(eratosthenes2(limt))
count = len(result)
# Measure and print the elapsed time
elapsed = datetime.datetime.now() - startTime
print(f" Found {count} primes in {elapsed}s\n")

# Record the current time
startTime = datetime.datetime.now()
result = list(primes_upto(limt))
count = len(result)
# Measure and print the elapsed time
elapsed = datetime.datetime.now() - startTime
print(f" Found {count} primes in {elapsed}s\n")

# Record the current time
startTime = datetime.datetime.now()
result = list(iprimes_upto(limt))
count = len(result)
# Measure and print the elapsed time
elapsed = datetime.datetime.now() - startTime
print(f" Found {count} primes in {elapsed}s\n")

# Record the current time
startTime = datetime.datetime.now()
result = list(primes2(limt))
count = len(result)
# Measure and print the elapsed time
elapsed = datetime.datetime.now() - startTime
print(f" Found {count} primes in {elapsed}s\n")

# Record the current time
startTime = datetime.datetime.now()
result = list(iprimes2(limt))
count = len(result)
# Measure and print the elapsed time
elapsed = datetime.datetime.now() - startTime
print(f" Found {count} primes in {elapsed}s\n")

# Record the current time
startTime = datetime.datetime.now()
result = list(primes_upto2(limt))
count = len(result)
# Measure and print the elapsed time
elapsed = datetime.datetime.now() - startTime
print(f" Found {count} primes in {elapsed}s\n")
