#!/bin/bash

export DEBIAN_FRONTEND=noninteractive
export UCF_FORCE_CONFFOLD=1

if [ -d "${HOME}/.pyenv" ]; then
    # shellcheck disable=SC1091
  . "${HOME}/.paths"
  echo "Updating pyenv..."
  pyenv update
  echo
fi


# update the dotfiles
pushd "${HOME}/dotfiles" || exit 1
  echo "Updating dotfiles..."
  ./forcepull.sh
  echo "DOTFILES updated"
  echo
popd || exit 1

if [ -e "${HOME}/.lektrix.branch" ]; then
  echo "Updating lektrix..."
    "${HOME}/lektrix/lektrix" --update
    echo "LEKTRIX updated"
fi

if [ -e "${HOME}/.wizwtr.branch" ]; then
  echo "Updating wizwtr..."
    "${HOME}/wizwtr/wizwtr" --update
    echo "WizWTR updated"
fi

if [ -e "${HOME}/.kimnaty.branch" ]; then
  echo "Updating KIMNATY..."
    "${HOME}/kimnaty/kimnaty" --update
  echo "KIMNATY updated"
  echo
fi

if [ -e "${HOME}/.bups.branch" ]; then
  echo "Updating BUPS..."
    "${HOME}/bups/bups" --update
  echo "BUPS updated"
  echo
fi

if [[ -f /boot/dietpi/dietpi-update ]]; then
  echo "...Updating Diet-Pi"
  echo "===================="
  sudo /boot/dietpi/dietpi-update 1
  echo
fi

echo
echo "..Updating"
echo "===================="
logger -p local1.info -t sysupdate Update
sudo DEBIAN_FRONTEND=noninteractive apt-get -yq update

echo
echo "..Listing"
echo "===================="
dpkg --get-selections | xargs apt-cache policy {} | grep -1 Installed | \
     sed -r 's/(:|Installed: |Candidate: )//' | uniq -u | tac | \
     sed '/--/I,+1 d' | tac | sed '$d' | sed -n 1~2p
echo

echo
echo "..Cleaning"
echo "===================="
logger -p local1.info -t sysupdate AutoClean
sudo DEBIAN_FRONTEND=noninteractive apt-get -yq autoclean

echo
echo "..Removing"
echo "===================="
logger -p local1.info -t sysupdate AutoRemove
sudo DEBIAN_FRONTEND=noninteractive apt-get -yq autoremove

echo
echo "..Upgrading"
echo "===================="
logger -p local1.info -t sysupdate Upgrade
sudo DEBIAN_FRONTEND=noninteractive apt-get -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -yquV dist-upgrade

echo
if [[ -f /usr/local/bin/pihole ]]; then
  echo
  echo "..Upgrading Pi-hole"
  echo "===================="
    sudo pihole -up
    sudo pihole -g
    # ${HOME}uss_cygnus/vincent.sh
    pihole status
fi

echo
echo
echo "System Update for $(hostname) is finished"
echo
