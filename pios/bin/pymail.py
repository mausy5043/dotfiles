#!/usr/bin/env python3

import argparse
import configparser
import os
import smtplib
import time
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

HOME = os.path.expanduser("~")
NODE = os.uname()[1]

parser = argparse.ArgumentParser(description="Send mail to the outside world.")
parser.add_argument(
    "-s",
    "--subject",
    type=str,
    help="The subject of the message.",
)
parser.add_argument(
    "-f",
    "--file",
    type=str,
    help="The path to a textfile to be used for the body of the message.",
)
OPTION = parser.parse_args()

# to be able to send mail the user must have a file
# called ~/.mailrc which contains sender and recipient addresses
mailrc = configparser.ConfigParser()
mailrc.read(f"{HOME}/.mailrc")
GMAIL_ADDRESS = mailrc["GMAIL"]["ADDRESS"]
GMAIL_PASSWORD = mailrc["GMAIL"]["PASSWORD"]
TO_MAIL = mailrc["GMAIL"]["RECIPIENT"]
FROM_MAIL = f"{NODE} <{GMAIL_ADDRESS}>"


def wait_for_network(diff_time):
    end_time = time.time() + diff_time
    network_is_up = False

    while (not network_is_up) and (diff_time > 0):
        sock = None
        try:
            sock = smtplib.SMTP_SSL("smtp.gmail.com", "465")
            sock.quit()
            network_is_up = True
        except BaseException:
            network_is_up = False

        if not network_is_up:
            # wait
            time.sleep(30)
        diff_time = end_time - time.time()
    return network_is_up


if __name__ == "__main__":
    network_up = wait_for_network(600)
    if network_up:
        if not OPTION.subject:
            OPTION.subject = "Forgot the subject"

        body = "No body"
        try:
            if OPTION.file:
                with open(OPTION.file) as fp:
                    body = fp.read()
        except BaseException:
            pass

        msg = MIMEMultipart()
        msg.attach(MIMEText(body, "plain"))

        msg["Subject"] = OPTION.subject
        msg["From"] = FROM_MAIL
        msg["To"] = TO_MAIL

        s = smtplib.SMTP_SSL("smtp.gmail.com", "465")
        s.login(GMAIL_ADDRESS, GMAIL_PASSWORD)
        s.sendmail(FROM_MAIL, TO_MAIL, msg.as_string())
        s.quit()
        print(f"Mail {OPTION.subject} sent.")
