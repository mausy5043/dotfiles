#!/bin/bash

if [[ -e "$1" ]]; then
  touch "$1"
  pushd "$1" || exit
    find . -exec touch {} \;
  popd || exit
fi
