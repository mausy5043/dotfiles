#!/bin/bash

markthebench()
{
  tf=$1
  sync
  echo "Write test"
  echo "----------"
  #echo "with RAM cache"
  #dd if=/dev/zero of="$tf" bs=1M count=128
  #echo "----------"
  echo "with RAM cache and final sync"
  dd if=/dev/zero of="$tf" bs=1M count=820 conv=fdatasync
  echo "----------"
  #echo "without RAM cache"
  #dd if=/dev/zero of="$tf" bs=1M count=128 oflag=dsync
  echo "Read Test"
  echo "----------"
  sudo sh -c "sync && echo 3 > /proc/sys/vm/drop_caches"
  dd if="$tf" of=/dev/null bs=1M count=820 |tail -n 1
  echo "----------"
  echo "Cleanup"
  rm "$tf"
  echo
}

uname -a
echo
echo "################"
echo "# CPU test     #"
echo "################"
echo
dd if=/dev/zero bs=1M count=1024 | md5sum
echo

echo "################"
echo "# Local Disk   #"
echo "################"
echo
markthebench ~/test.tmp

echo "################"
echo "# NFS          #"
echo "################"
echo
markthebench ~/bin/test.tmp

echo "################"
echo "# /tmp         #"
echo "################"
echo
markthebench /tmp/test.tmp

if [ -d ~/torrent/ ]; then
 echo "################"
 echo "# torrent-disk #"
 echo "################"
 echo
 markthebench ~/torrent/test.tmp
fi

#
