#!/bin/sh



sudo apt-get update >/dev/null 2>&1


temp_file=$(mktemp)
dpkg --get-selections | \
     xargs apt-cache policy {} | \
     grep -1 Installed > "$temp_file"

~/bin/chkpkgs.py "$temp_file"
rm "$temp_file"
