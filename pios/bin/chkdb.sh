#!/bin/bash

DATABASE_COMMON_PATH="/srv/rmt/_databases"
DATABASE_COMMON_BACKUP_PATH="${DATABASE_COMMON_PATH}/backup"
DATABASE_EXT="sqlite3"

declare -a databases=(
  "kimnaty/kimnaty"
  "lektrix/lektrix"
  "bups/upsdata")

backup_database()
{
  DB_FILE="${1}"
  cp -a "${DATABASE_COMMON_PATH}/${DB_FILE}.${DATABASE_EXT}" "${DATABASE_COMMON_BACKUP_PATH}"
}

for DB in "${databases[@]}"; do
  backup_database "${DB}"
  rclone sync /srv/rmt/_databases/ remote:raspi/_databases/
done
