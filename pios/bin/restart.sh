#!/bin/bash

lg()
{
  msg="${1}"
  logger -p 2 -t "restart" "${msg}"
  echo "${msg}"
}

um()
{
  volume="$1"
  mount | grep " on $volume type" >/dev/null
  # shellcheck disable=SC2181
  if [ $? -eq 0 ]; then
    sudo umount -l "$volume" >/dev/null
    # shellcheck disable=SC2181
    if [ $? -eq 0 ]; then
      echo "[OK]"
      lg "$volume unmounted"
    else
      echo "[FAIL]"
      lg "$volume unmount FAILED"
    fi
  else
    echo
    echo "[ $volume not found ]"
  fi
}

REBOOT=true

while [[ $# -gt 0 ]]; do
  case "$1" in
    --noreboot )   REBOOT=false; shift ;;
    -- ) shift; break ;;
    * ) shift ;;
  esac
done

if [ -e /usr/bin/qbittorrent-nox ]; then
  echo "Stopping Torrent..."
    sudo systemctl stop qbittorrent.service
    lg "qBitTorrent stopped"
    sudo systemctl stop smbd.service
    lg "Samba stopped"
    sudo systemctl stop nfs-kernel-server.service
    lg "NFS stopped"
fi

if [ -e "/home/pi/lektrix" ]; then
  echo "Stopping lektrix..."
    "/home/pi/lektrix/lektrix" --stop
    lg "LEKTRIX stopped"
fi

if [ -e "/home/pi/wizwtr" ]; then
  echo "Stopping wizwtr..."
    "/home/pi/wizwtr/wizwtr" --stop
    lg "WIZWTR stopped"
fi

if [ -e "/home/pi/kimnaty" ]; then
  echo "Stopping KIMNATY..."
    "/home/pi/kimnaty/kimnaty" --stop
    lg "KIMNATY stopped"
fi

if ! $REBOOT; then
  echo "Not rebooting yet (as requested)."
  exit 0
fi


echo "Syncing disks..."
sync; sync; sleep 10

echo -n "Unmounting DATABASES "
um /srv/data
um /srv/databases

echo -n "Unmounting FILES "
um /mnt/dietpi_userdata

echo -n "Unmounting CONFIG "
um /srv/usb

echo -n "Unmounting MEDIA  "
um /srv/hdd

lg "***********************************"
lg " $(hostname) IS REBOOTING !!"
lg "***********************************"

echo "Logging you out and rebooting..."
sleep 10
sudo systemctl reboot
