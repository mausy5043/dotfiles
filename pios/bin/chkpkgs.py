#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser(description='Check package versions.')
parser.add_argument('filename', type=str, help='The filename containing the package list')
args = parser.parse_args()

fi = args.filename
with open(fi) as f:
    pkgs = f.read().splitlines()

for line in range(0, len(pkgs), 4):
    pack = pkgs[line].strip(" \t\n:")
    spc = 32 - len(pack)
    if spc < 0:
        spc = 2
    spc0 = " " * spc
    installed = pkgs[line + 1].strip(" \t\n").split()[1]
    candidate = pkgs[line + 2].strip(" \t\n").split()[1]
    if installed != "(none)" and installed != candidate:
        print(f"{pack}{spc0}\033[35m installed\033[00m   -->  {candidate}")
