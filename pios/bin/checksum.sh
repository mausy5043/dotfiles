#!/bin/bash

cmd="${1}"
addir="${2}"
chkfile="$HOME/.config/check.sum"

create_list()
{
  if [[ ! -d "${HOME}/.config" ]]; then
    mkdir "${HOME}/.config"
  fi
  nice find /boot -exec shasum -a 1 {} \; > "${chkfile}"
}

add_list()
{
  dir="${1}"
  nice find "${dir}"  -exec shasum -a 1 {} \; >> "${chkfile}"
}

check_list()
{
  nice shasum -c "${chkfile}" |grep -v "OK"
}

remove_entry()
{
  pattern="${1}"
  echo "$pattern"
  sed -i "\|${pattern}|d" "${chkfile}"
}

case "$cmd" in
	"-create")
		create_list
		;;
	"-check")
		check_list
		;;
	"-add")
		add_list "${addir}"
		;;
	"-remove")
		remove_entry "${addir}"
		;;
	*)
		echo "-create or -check or -add <dir>"
		exit 1
		;;
esac
