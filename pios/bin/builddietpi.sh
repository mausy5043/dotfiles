#!/bin/bash

# rebuild existing dietpi installation

SERVICE_DIR="/opt/mod-dietpi"
MYHOME="${HOME}"

# set-up a persistent storage
if [ -d "${SERVICE_DIR}" ]; then
  # clear out the dustbunnies
  sudo rm -r "${SERVICE_DIR}"
fi
sudo mkdir -p "${SERVICE_DIR}/mod-dietpi"

# prepare WIFI-settings in case they are needed
sudo cp -r "${MYHOME}/.config/dietpi-wifi.txt" "${SERVICE_DIR}"

# throw away any previous installers
if [ -d /tmp/mod-dietpi ]; then
  rm -rf /tmp/mod-dietpi
fi

#ssid=$(grep ssid ${MYHOME}/.config/wpa.conf | awk -F= '{print $2}')
#psk=$(grep psk ${MYHOME}/.config/wpa.conf | awk -F= '{print $2}')
#{
#  echo "aWIFI_SSID[0]=${ssid}"
#  echo "aWIFI_KEY[0]=${psk}"
#} | sudo tee /boot/dietpi-wifi.txt > /dev/null

# prepare for reboot
# shellcheck disable=SC2086
${MYHOME}/bin/restart.sh --noreboot

cd /tmp || exit 1
  git clone https://github.com/Mausy5043/mod-dietpi.git || exit 1
  cd mod-dietpi || exit 1
    sudo ./mod-dietpi.sh "$@" --service "${SERVICE_DIR}" || exit 1
exit 0
