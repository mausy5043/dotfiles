#!/bin/sh

# compile and install bleeding edge bluetooth support

if [ -z "$STY" ]; then exec screen -dm -S buildbluez /bin/bash "$0"; fi

sudo cp /etc/apt/sources.list /etc/apt/sources.list.d/deb-src.list
sudo sed -i 's/^deb /deb-src /' /etc/apt/sources.list.d/deb-src.list
sudo apt-get update
sudo apt-get -y build-dep bluez

sudo apt-get -y install python3-docutils

cd /tmp || exit
git clone git://git.kernel.org/pub/scm/libs/ell/ell.git
git clone https://github.com/bluez/bluez.git
cd bluez || exit
./bootstrap
./configure --prefix=/usr --mandir=/usr/share/man --sysconfdir=/etc --localstatedir=/var

make && sudo make all && sudo make install

sudo systemctl daemon-reload
sudo systemctl restart bluetooth.service
