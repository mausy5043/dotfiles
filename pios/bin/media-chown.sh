#!/bin/bash

pushd /mnt/media/Shared || exit
  # own everything
  echo -n "."
  sudo chown -R 1000:users "./Movies"
  echo -n "."
  sudo chown -R 1000:users "./TV series"

  # set correct permissions
  echo -n "."
  chmod -R 744 "./Movies"
  echo -n "."
  chmod -R 744 "./TV series"

  # remove Apple stuff
  echo -n "."
  find "./Movies" -name \.AppleDouble -exec rm -rf {} \;
  echo -n "."
  find "./Movies" -name "._*" -exec rm -rf {} \;
  echo -n "."
  find "./TV series" -name \.AppleDouble -exec rm -rf {} \;
  echo -n "."
  find "./TV series" -name "._*" -exec rm -rf {} \;
  echo ""
popd || exit
