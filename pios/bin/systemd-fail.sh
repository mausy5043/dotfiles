#!/bin/bash
# Send a mail if a systemd service fails
# ExecStart=/usr/local/bin/failure-notification.sh %i
#
#ref:  https://wiki.archlinux.org/title/systemd#Notifying_about_failed_services

FAILED_SERVICE="${1}"
ROOT_FILE="/tmp/${FAILED_SERVICE}-fail.msg"
# Prevent fast firing; check if we sent a message in the past X hours (see end of script)
if compgen -G "${ROOT_FILE}.*" > /dev/null; then
    exit 0
fi

TMPFILE=$(mktemp "${ROOT_FILE}.XXXX")

# Prepare email
systemctl status "${FAILED_SERVICE}" > "${TMPFILE}" 2>&1
echo "" >> "${TMPFILE}"

# sed -i '/^Copy/ d' "${TMPFILE}


# Send the email, provided the file /tmp/failmail.stop does not exist.
if [ ! -f /tmp/failmail.stop ]; then
  /usr/local/bin/pymail.py -s "Service ${FAILED_SERVICE} failed!" -f "${TMPFILE}"
fi

# Prevent fast firing; wait with deleting the tempfile for X hours so we don't keep sending it.
sleep 14400   # 4 hours
rm "${TMPFILE}"
