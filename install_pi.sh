#!/usr/bin/env bash

HERE=$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)
FILES="${HERE}/pios/*"

touch "$HOME/.alias"
touch "$HOME/.funcs"
touch "$HOME/.paths"

for filepath in $FILES; do
  file=$(basename "$filepath")
  # Files
  echo -n "- Processing .$file  "
  if [ -e "${HOME}/.${file}" ]; then
    if [ -L "${HOME}/.${file}" ]; then
      echo "  .$file exists as a link. Ignoring."
    else
      echo "  .$file exists as a regular file."
      echo -n "      Moving out of the way: "
      mv -v "${HOME}/.${file}" "${HOME}/.${file}.bak"
      echo -n "      Creating link        : "
      ln -sv "${filepath}" "${HOME}/.${file}"
    fi
  fi
done

sudo mkdir -p /usr/local/bin
if [ -f /usr/local/bin/failure-notification.sh ]; then
  sudo rm /usr/local/bin/failure-notification.sh
fi
sudo ln -s "$HERE/pios/bin/systemd-fail.sh" /usr/local/bin/failure-notification.sh
if [ -f /usr/local/bin/pymail.py ]; then
  sudo rm /usr/local/bin/pymail.py
fi
sudo ln -s "$HERE/pios/bin/pymail.py" /usr/local/bin/pymail.py
# install a service that sends a mail if a service fails
sudo mkdir -p /etc/systemd/system/service.d
sudo cp "$HERE/common/systemd/10-all.conf" /etc/systemd/system/service.d/
sudo cp "$HERE/common/systemd/failure-handler@.service" /etc/systemd/system/
if [ -d /etc/systemd/system/failure-handler@.service.d/ ]; then
  sudo rm -r /etc/systemd/system/failure-handler@.service.d/
fi
sudo mkdir /etc/systemd/system/failure-handler@.service.d/
sudo ln -s /dev/null /etc/systemd/system/failure-handler@.service.d/10-all.conf

echo ""
echo "Correct and reload system services (0644)"
sudo chmod -R -x /etc/systemd/system
sudo systemctl daemon-reload

sudo apt-get -y install bind9-dnsutils

echo; whoami;echo
# execute .paths
/bin/bash "$HOME/.paths"
# record the result in the log
ls -al "$HOME"
echo "PATH=$PATH"
echo
