#!/usr/bin/env python3

import sys


def prime_factors(n):
    factors = []
    divisor = 2

    while n > 1:
        while n % divisor == 0:
            factors.append(divisor)
            n //= divisor
        divisor += 1

    return factors


# Example usage:
num = int(sys.argv[1])
print("Prime factors of", num, "are:", prime_factors(num))
