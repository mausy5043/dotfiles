#!/usr/bin/env python

# Generate prime numbers
# Based on an article from http://dunningrb.wordpress.com/

import datetime
import sys


# Function to print message
def print_prime(x):
    print(f" Prime : {x}")


# Function to search for prime numbers
# within number range
def find_primes(u_limit):
    cnt = 0
    candidate = 3
    while candidate <= u_limit:
        trial_divisor = 2
        prime = 1  # assume it's prime
        while trial_divisor**2 <= candidate and prime:
            if candidate % trial_divisor == 0:
                prime = 0  # it isn't prime
            trial_divisor += 1
        if prime:
            print_prime(candidate)
            cnt += 1
        candidate += 2
    return cnt


# Check if the script was called with a
# parameter. Use that as the upper limit
# of numbers to search
upper_limit = 1000 if len(sys.argv) != 2 else int(sys.argv[1])

# Record the current time
startTime = datetime.datetime.now()

# Start the process
print("")
print("Starting ...")
print("")
count = find_primes(upper_limit)
print("")

# Measure and print the elapsed time
elapsed = datetime.datetime.now() - startTime
print(f" Found {count} primes in {elapsed}")
print("")
