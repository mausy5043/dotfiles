#!/usr/bin/env python3

"""Display dependency differences in the various configuration files of a project"""

import argparse
import os
import warnings

import tomli as tl
import yaml


def my_warning(msg, *args, **kwargs):
    # ignore everything except the message
    return str(msg) + "\n"


warnings.formatwarning = my_warning

# fmt: off
parser = argparse.ArgumentParser(description="Execute the dependency differ.")
parser.add_argument("-r", "--txt",
                          type=str,
                          help="path to the `requirements.txt` file relative from where you are now, if not in this location"
                          )
parser.add_argument("-t", "--tml",
                          type=str,
                          help="path to the `pyproject.toml` file relative from where you are now, if not in this location"
                          )
parser.add_argument("-y", "--yml",
                          type=str,
                          help="path to the `environment.yml` file relative from where you are now, if not in this location"
                          )
OPTION = parser.parse_args()
# fmt: on

# set-up some constants
_WORK_DIR: str = os.getcwd()
_TML_FILE: str = f"{_WORK_DIR}/pyproject.toml"
_TXT_FILE: str = f"{_WORK_DIR}/requirements.txt"
_YML_FILE: str = f"{_WORK_DIR}/environment.yml"


def check_for_file(fname):
    if not os.path.isfile(fname):
        warnings.warn(f"- {fname} not found", stacklevel=2)
        fname = ""
    return fname


def process_toml():
    """Read package requirements from a pyproject.toml file"""
    _toml_deps: dict = {}
    _toml_list: list = []
    try:
        with open(_TML_FILE, mode="rb") as _fp:
            TOML_CONTENTS = tl.load(_fp)
            _toml_list = TOML_CONTENTS["project"]["dependencies"]
    except KeyError:
        pass
    for item in _toml_list:
        # Split on the first occurrence of '==' or '>=' or '<=' or similar
        for sep in ["==", ">=", "<=", ">", "<", "~=", "!="]:
            if sep in item:
                package, version = item.split(sep, 1)
                _toml_deps[package.strip()] = version.strip()
                break
        else:
            # If no version specifier is found
            _toml_deps[item] = ""
    return _toml_deps


def process_txt():
    """Read package requirements from a requirements.txt file"""
    _txt_deps: dict = {}
    with open(_TXT_FILE) as _fp:
        for line in _fp:
            # Strip whitespace and skip comments or empty lines
            line = line.strip()
            if not line or line.startswith("#"):
                continue

            # Split on the first occurrence of '==' or '>=' or '<=' or similar
            for sep in ["==", ">=", "<=", ">", "<", "~=", "!="]:
                if sep in line:
                    package, version = line.split(sep, 1)
                    _txt_deps[package.strip()] = version.strip()
                    break
            else:
                # If no version specifier is found
                _txt_deps[line] = ""

    return _txt_deps


def process_yaml():
    """Read package requirements from a environment.yml file"""
    _yml_deps: dict = {}
    with open(_YML_FILE) as _fp:
        env_data = yaml.safe_load(_fp)

    # Look for the dependencies section
    dependencies = env_data.get("dependencies", [])

    for dep in dependencies:
        if isinstance(dep, str):
            # Handle dependencies listed as strings (e.g., "numpy=1.21.0")
            for sep in ["==", ">=", "<=", ">", "<", "~=", "!=", "="]:
                if sep in dep:
                    package, version = dep.split(sep, 1)
                    _yml_deps[package.strip()] = version.strip()
                    break
            else:
                # If no version specifier is found
                _yml_deps[dep] = ""
        elif isinstance(dep, dict):
            # Handle nested dependencies (e.g., pip sub-section)
            for _, pkgs in dep.items():
                if isinstance(pkgs, list):
                    for pkg in pkgs:
                        for sep in ["==", ">=", "<=", ">", "<", "~=", "!=", "="]:
                            if sep in pkg:
                                package, version = pkg.split(sep, 1)
                                _yml_deps[package.strip()] = version.strip()
                                break
                        else:
                            _yml_deps[pkg] = ""

    return _yml_deps


def main():
    """."""
    # process the three files
    tml_deps: dict = {}
    txt_deps: dict = {}
    yml_deps: dict = {}
    if _TML_FILE:
        tml_deps = process_toml()
    if _TXT_FILE:
        txt_deps = process_txt()
    if _YML_FILE:
        yml_deps = process_yaml()

    for key, value in tml_deps.items():
        if key in txt_deps and txt_deps[key] != value:
            print(
                f"Mismatching version of `{key}`: {_TML_FILE} wants {value} vs "
                f"{txt_deps[key]} in {_TXT_FILE}"
            )

    for key, value in txt_deps.items():
        if key in yml_deps and yml_deps[key] != value:
            print(
                f"Mismatching version of `{key}`: {_TXT_FILE} wants {value} vs "
                f"{yml_deps[key]} in {_YML_FILE}"
            )

    for key, value in yml_deps.items():
        if key in tml_deps and tml_deps[key] != value:
            print(
                f"Mismatching version of `{key}`: {_YML_FILE} wants {value} vs "
                f"{tml_deps[key]} in {_TML_FILE}"
            )


if __name__ == "__main__":
    if OPTION.tml:
        _TML_FILE = f"{_WORK_DIR}/{OPTION.tml}"
    if OPTION.txt:
        _TXT_FILE = f"{_WORK_DIR}/{OPTION.txt}"
    if OPTION.yml:
        _YML_FILE = f"{_WORK_DIR}/{OPTION.yml}"
    _TML_FILE = check_for_file(_TML_FILE)
    _TXT_FILE = check_for_file(_TXT_FILE)
    _YML_FILE = check_for_file(_YML_FILE)
    # print(_TML_FILE)
    # print(_TXT_FILE)
    # print(_YML_FILE)
    main()
