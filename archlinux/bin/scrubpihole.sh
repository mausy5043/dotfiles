#!/bin/bash

DB="/etc/pihole/pihole-FTL.db"

if [ -e "${DB}" ]; then
#   echo "These entries will be removed from the database:"
#   sudo sqlite3 "${DB}" "SELECT * FROM network WHERE lastQuery < $(echo "$(date '+%s') - 30 * 24 * 3600" |bc);"
#   echo ""
#  sudo sqlite3 "${DB}" "DELETE FROM network WHERE lastQuery < $(echo "$(date '+%s') - 30 * 24 * 3600" |bc)"
  sudo ip -s -s neigh flush all
  echo ""
  echo ""
  echo "These entries remain:"
  sudo sqlite3 "${DB}" "SELECT * FROM network"
fi
echo ""
