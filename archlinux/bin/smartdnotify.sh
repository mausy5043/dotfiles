#!/bin/bash

TMPFILE=$(mktemp "/tmp/smartd.msg.XXXX")

# Send email
{
  echo "${SMARTD_FULLMESSAGE}"
  echo "${SMARTD_DEVICEINFO}"
  echo "${SMARTD_FAILTYPE}"
  echo ""
  # add device info
  smartctl -q errorsonly -x "${SMARTD_DEVICE}"
  smartctl -l xerror "${SMARTD_DEVICE}"
} > "${TMPFILE}"

sed -i '/^Copy/ d' "${TMPFILE}"

/usr/local/bin/pymail.py -s "SMARTD report: ${SMARTD_MESSAGE}" -f "${TMPFILE}"

rm "${TMPFILE}"
