#!/bin/sh

#for i in {1..5}; do
#  cat "/sys/class/hwmon/hwmon0/temp${i}_label" "/sys/class/hwmon/hwmon0/temp${i}_input" | tr '\n' ' '; echo ""
#done
echo

sensors

echo
df -h

echo
free -h

echo
uptime

echo
top -bn 1 |grep "Cpu(s)" | awk '{print "user: " $2+$6 "%  syst: " $4+$12+$14+$16 "%  wait: " $10 "%  idle: "$8"%    Total: " $2+$4+$6+$8+$10+$12+$14+$16 "%"}'

echo
a=$(sudo smartctl --all /dev/sda |grep '194 Temperature_Celsius' |awk '{print $10}')
b=$(sudo smartctl --all /dev/sdb |grep '194 Temperature_Celsius' |awk '{print $10}')
c=$(sudo smartctl --all /dev/sdc |grep '194 Temperature_Celsius' |awk '{print $10}')
d=$(sudo smartctl --all /dev/sdd |grep '194 Temperature_Celsius' |awk '{print $10}')
e=$(sudo smartctl --all /dev/sde |grep '194 Temperature_Celsius' |awk '{print $10}')
echo "sda: $a || sdb: $b || sdc: $c || sdd: $d || sde: $e degC"

echo
