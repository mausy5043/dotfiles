#!/bin/bash

updatables="/tmp/pkgs.txt"

## DEBIAN:
#dpkg --get-selections | xargs apt-cache policy {} | grep -1 Installed > "${updatables}"
#chkpkgs.py

## ARCH Linux
sudo pacman -Syy --noprogressbar
checkupdates > "${updatables}"
if [[ ! "$(wc -l "${updatables}" | cut -d' ' -f 1)" == "0" ]]; then
  #echo "" >> "${updatables}"
  #echo "yay -Qu :" >> "${updatables}"
  #yay -Qu >> "${updatables}"
  pymail.py -s "$(hostname) has updates available" -f "${updatables}"
fi

rm "${updatables}"
