#!/bin/sh

if [ -e /bin/journalctl ]; then
  echo "_____-----_____"
  echo "7..7:"
    # journalctl --no-pager -p4..4 --boot 0 -n 20
    journalctl --since 00:00 --no-pager -p7..7 |tail -n 30
  echo "_____-----_____"
  echo "6..6:"
    journalctl --since 00:00 --no-pager -p6..6 |tail -n 30
  echo "_____-----_____"
  echo "5..5:"
    journalctl --since yesterday --no-pager -p5..5 |tail -n 30
  echo "_____-----_____"
  echo "4..4:"
    journalctl --since yesterday --no-pager -p4..4 |tail -n 30
  echo "_____-----_____"
  echo "3..3:"
    journalctl --since yesterday --no-pager -p3..3 |tail -n 30
  echo "_____-----_____"
  echo "2..2:"
    journalctl --b 0 --no-pager -p2..2 |tail -n 30
  echo "_____-----_____"
  echo "1..1:"
    journalctl --b 0 --no-pager -p1..1 |tail -n 30
  echo "_____-----_____"
  echo "0..0:"
    journalctl --b 0 --no-pager -p0..0 |tail -n 30
fi
