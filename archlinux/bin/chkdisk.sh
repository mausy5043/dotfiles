#!/bin/bash

for i in "a" "b" "c" "d" "e"; do
  if [ -e "/dev/sd$i" ]; then
    echo "$i"
    echo "+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+="
    # sudo smartctl -x /dev/sd$i
    sudo smartctl -q errorsonly -x "/dev/sd$i"
    sudo smartctl -l xerror "/dev/sd$i"
    echo ""
    echo "Press any key to continue"
    while true; do
      if read -r -t 3 -n 1; then
        break
      fi
    done
    echo "+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+="
  fi
done
