#!/bin/bash
event="$1"
device="$2"

TMPFILE=$(mktemp "/tmp/mdadm.msg.XXXX")

echo "Event  : ${event} " > "${TMPFILE}"
echo "Device : ${device} " >> "${TMPFILE}"

/usr/local/bin/pymail.py -s "MDADM message" -f "${TMPFILE}"

rm "${TMPFILE}"
