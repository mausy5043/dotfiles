#!/bin/sh

DUCKFILE=/tmp/duck.log
DOMAINS=""
TOKEN=""
# shellcheck disable=SC1091
. /home/beheer/.config/.duckdns.secret

echo url="https://www.duckdns.org/update?domains=${DOMAINS}&token=${TOKEN}&verbose=true" | curl -k -Ss -o ${DUCKFILE} -K -
echo >> ${DUCKFILE}
date >> ${DUCKFILE}
