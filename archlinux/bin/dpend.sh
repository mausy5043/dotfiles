#!/bin/bash


DOTFILE=$(mktemp "/tmp/out.dot.XXXX")
DEPStemp=$(mktemp -d "/tmp/out.pdf.XXXX")

PARAMETER="${1}"

CALLERID="${2}"
#DEPSpub=deps
#DEPS=/srv/array1/public/deps
CALLERDIR="${HOME}/Downloads"
DESTINATION="${CALLERID}:${CALLERDIR}"

echo "Looking for : ${PARAMETER}"
echo

# see if `which $1` succeeds; then $1 is a filename; else $1 is a packagename
if FILE=$(which "${PARAMETER}" 2>/dev/null) ; then
  #  you passed an executable name use -Qo to find the package name
  PACKAGE=$(pacman -Qo "${FILE}" | awk '{print $5}')
  echo "${FILE} is owned by ${PACKAGE}"
else
  if FILE=$(whereis "${PARAMETER}" 2>/dev/null); then
    PACKAGE=$(pacman -Qo "${PARAMETER}" | awk '{print $5}')
    if [ -z "${PACKAGE}" ]; then
      exit 1
    fi
    echo "${PARAMETER} is owned by ${PACKAGE}"
  else
    PACKAGE="${PARAMETER}"
  fi
fi

# query the package info

# Display information about the package
echo; echo "====="; echo
pacman -Qi "${PACKAGE}" || exit 1

# Create the dependency tree in .dot format
echo "${PACKAGE} requires:"
pactree -c "${PACKAGE}"
pactree -g "${PACKAGE}" > "${DOTFILE}"
# Convert the dot-file to .pdf
dot -T pdf -o "$DEPStemp/${PACKAGE}-requires.pdf" "${DOTFILE}"
chmod ugo+rw "$DEPStemp/${PACKAGE}-requires.pdf"

echo
echo "Packages requiring ${PACKAGE}"
pactree -cr "${PACKAGE}"
pactree -gr "${PACKAGE}" > "${DOTFILE}"

# Convert the dot-file to .pdf
dot -T pdf -o "$DEPStemp/requires-${PACKAGE}.pdf" "${DOTFILE}" && rm "${DOTFILE}"
chmod ugo+rw "$DEPStemp/requires-${PACKAGE}.pdf"

echo
  echo "Uploading graphic trees"
  scp "$DEPStemp/${PACKAGE}-requires.pdf" "${DESTINATION}/${PACKAGE}-requires.pdf" && rm "$DEPStemp/${PACKAGE}-requires.pdf"
  scp "$DEPStemp/requires-${PACKAGE}.pdf" "${DESTINATION}/requires-${PACKAGE}.pdf" && rm "$DEPStemp/requires-${PACKAGE}.pdf"
rmdir --ignore-fail-on-non-empty "${DEPStemp}"
