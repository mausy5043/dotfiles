#!/usr/bin/env python3

import socket
import time


def getIpAddresses():
    addrList = socket.getaddrinfo(socket.gethostname(), None)
    ipList = []
    for _item in addrList:
        ipList.append(_item[4][0])

    return ipList


diff_time = 9.0
end_time = time.time() + diff_time
network_is_up = False

while (not network_is_up) and (diff_time > 0):
    lijstje = getIpAddresses()
    print(lijstje)

    for _item in lijstje:
        print(_item)
        if "192.168" in _item:
            network_is_up = True

    if not network_is_up:
        time.sleep(3)

    diff_time = end_time - time.time()
