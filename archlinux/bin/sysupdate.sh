#!/bin/bash

pushd ~/dotfiles || exit
  ./forcepull.sh
popd || exit

if [[ -f /usr/bin/pihole ]]; then
  echo
  echo "..Upgrading Pi-hole"
  echo "=============="
    scrubpihole.sh
    sudo pihole -g
    pihole status
fi

echo "...Updating"
echo "=============="
logger -p local1.info -t sysupdate "===Update==="
yay -Sy

echo
echo "...Checking"
echo "=============="
logger -p local1.info -t sysupdate "===Check==="
yay -Sc --noconfirm

echo
systemctl --failed

echo
echo "...List of removables"
echo "====================="
logger -p local1.info -t sysupdate "===Orphan check==="
yay -Qtd


echo
echo "...Cleaning"
echo "==========="
sleep 15
logger -p local1.info -t sysupdate "===Clean==="
# yay -Rns $(pacman -Qtdq)
yay -Scc --noconfirm

echo "   Uninstalled packages:"
sudo paccache -ruk0
echo "   Old versions of packages:"
sudo paccache -rk2

echo
echo "...Upgrading"
echo "=============="
logger -p local1.info -t sysupdate "===Upgrade==="
yay -Syyu --noconfirm

echo
yay -Ps

echo
yay -Pw

~/bin/chkreboot.sh
