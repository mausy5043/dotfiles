#!/bin/bash

where="$1"

if [ "$where" = "" ]; then
  echo "Syntax: noapple <path>"
  exit 1
fi

function ditch {
  indir="$1"
  patrn="$2"
  echo "$patrn" in "$indir"
  find "$indir" -iname "$patrn" -exec rm -dRf "{}" \;
}

# remove Apple stuff
ditch "$where" "\.AppleDouble"
ditch "$where" "\.AppleDesktop"
ditch "$where" "\.AppleDB"
ditch "$where" "\.DS_Store"
