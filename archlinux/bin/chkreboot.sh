#!/bin/sh

NEXTLINE=0
FIND=""

for I in $(file /boot/vmlinuz-*); do
  if [ ${NEXTLINE} -eq 1 ]; then
    FIND="${FIND} ${I}"
    NEXTLINE=0
  else
    if [ "${I}" = "version" ]; then
      NEXTLINE=1;
    fi
  fi
done

if [ ! "${FIND}" = "" ]; then
  CURRENT_KERNEL=$(uname -r)
  if [ ! "${1}" = "-" ]; then
    echo
    echo "Kernel state"
    echo "Active   :  ${CURRENT_KERNEL}"
    echo "Installed:  ${FIND}"
  fi
  case "${FIND}" in
    *${CURRENT_KERNEL}*)
      echo
      ;;
    *)
      echo
      echo "=== Reboot required ==="
      echo
      ;;
  esac
fi
