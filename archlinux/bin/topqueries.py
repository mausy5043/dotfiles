#!/usr/bin/python
"""
Determine number of queries per site split out per local host
"""

import os
import sys


def reverse(dotted_string):
    """Reverse an URI"""
    return ".".join([element for element in reversed(dotted_string.split("."))])


if __name__ == "__main__":
    if len(sys.argv) == 3:
        if os.path.isfile(sys.argv[1]):
            with open(sys.argv[1]) as fin_handle:
                QUERIED_LINES = fin_handle.read().strip("\n").splitlines()
        else:
            print("queryfile does not exist")
            sys.exit(2)
        if os.path.isfile(sys.argv[2]):
            with open(sys.argv[2]) as fin_handle:
                BLOCKED_LINES = fin_handle.read().strip("\n").splitlines()
        else:
            print("blockfile does not exist")
            sys.exit(2)
    else:
        print(f"usage: {sys.argv[0]} <queryfile> <blockfile>")
        sys.exit(2)

    BLOCK_SET = set()
    IP_DICT = dict()
    for line in BLOCKED_LINES:
        BLOCK_SET.add(reverse(line.split()[4]))

    for line in QUERIED_LINES:
        items = line.split()
        event_dt = items[0]
        requestor_ip = items[7]
        signif_octet = requestor_ip.split(".")[-1].zfill(3)
        if ":" in signif_octet:
            signif_octet = "001"
        queried_site = items[5]
        rev_queried_site = reverse(queried_site)
        if signif_octet in IP_DICT:
            if rev_queried_site in IP_DICT[signif_octet]:
                IP_DICT[signif_octet][rev_queried_site] += 1
            else:
                IP_DICT[signif_octet][rev_queried_site] = 1
        else:
            IP_DICT[signif_octet] = {}
            IP_DICT[signif_octet][rev_queried_site] = 1

        # print(f"{event_dt}   {signif_octet}  {rev_queried_site}")
        # print(line)

    for ip_octet in sorted(IP_DICT):
        print("---")
        for site in sorted(IP_DICT[ip_octet]):
            blocked_flag = "BLOCKED" if site in BLOCK_SET else "       "
            print(f"{ip_octet} {blocked_flag} {str(IP_DICT[ip_octet][site]).rjust(5)}  {site}")
