#!/usr/bin/env bash
# Useful functions

# `tre` is a shorthand for `tree` with hidden files and color enabled, ignoring
# the `.git` directory, listing directories first. The output gets piped into
# `less` with options to preserve color and line numbers, unless the output is
# small enough for one screen.
tre() {
	tree -aC -I '.git|.idea|node_modules|bower_components' --dirsfirst "$@" | less -FRNX;
}

# `pp sep text` (prettyprint) splits a text `text` at the separator `sep`
pp() {
  sep=$1
  intext=$2
  tr "$sep" '\n' <<< "$intext" | sort;
}

# `indent` (obviously) indents its input by two spaces
indent() {
  sed 's/^/  /';
}

# `scan` get some basic info on a local host
scan() {
  # decimal value of fourth octet
  oct4=$1
  # first three octets of the LAN
  oct123=$(ip route |cut -d'.' -f1-3 |tail -1)
  sudo nmap -A -O -oN - "${oct123}.${oct4}"
}

# `testdns` check if local DNS service is up
testdns()
{
  dig +short @192.168.2.2 TXT CHAOS version.bind
}
