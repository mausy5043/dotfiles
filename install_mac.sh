#!/usr/bin/env bash

HERE=$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)
FILES="${HERE}/macos/*"

touch "$HOME/.alias"
touch "$HOME/.funcs"
touch "$HOME/.paths"

touch "$HOME/.zshenv"
touch "$HOME/.zshrc"
touch "$HOME/.zprofile"
touch "$HOME/.profile"

touch "$HOME/.bashrc"
touch "$HOME/.bash_profile"

touch "$HOME/.pylintrc"
touch "$HOME/.screenrc"

for filepath in $FILES; do
  file=$(basename "$filepath")
  # Files
  echo -n "- Processing .$file  "
  if [ -e "${HOME}/.${file}" ]; then
    if [ -L "${HOME}/.${file}" ]; then
      echo "  .$file exists as a link. Ignoring."
    else
      echo "  .$file exists as a regular file."
      echo -n "      Moving out of the way: "
      mv -v "${HOME}/.${file}" "${HOME}/.${file}.bak"
      echo -n "      Creating link        : "
      ln -sv "${filepath}" "${HOME}/.${file}"
    fi
  fi
done

mkdir -p "$HOME/.config"
mkdir -p "$HOME/.local/bin"
mkdir -p "$HOME/.sqlite3/bups"
mkdir -p "$HOME/.sqlite3/kimnaty"
mkdir -p "$HOME/.sqlite3/lektrix"
touch "$HOME/dwnloads"
mkdir -p "$HOME/coding/git/hub"
mkdir -p "$HOME/coding/git/lab"
ln -sv "$HOME/coding/git" "$HOME/git"
ln -sv "$HOME/coding/notgit" "$HOME/notgit"

# DIY:
# .config
# .condarc
# .mailrc
# .gitconfig
# .pypirc
# .pylintrc
# .zshrc
# ~/coding/notgit
# ~/dwnloads
