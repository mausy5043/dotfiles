![Static Badge](https://img.shields.io/badge/release-rolling-lightgreen)
[![Code style: Black](https://img.shields.io/badge/code%20style-Black-000000.svg)](https://github.com/psf/black)
[![Code style: Shellcheck](https://img.shields.io/badge/code%20style-ShellCheck-ccf)](https://github.com/koalaman/shellcheck)

# dotfiles

central store for all dotfiles
